---
title: API GraphQL
tags: [apprendre, comprendre, endpoint]
lang: fr
---

# API GraphQL

L'API GraphQL est ouvert au public à l'adresse suivante : [https://api.leafcad.com](https://api.leafcad.com). Toutes les requêtes doivent être formatées pour suivre le standard GraphQL. La plupart des requêtes doivent être effectuées en utilisant une clé d'API pour authentifier votre communauté.

Cette documentation sert de support à la référence d'API que vous pouvez consulter sur [https://graphql-docs.leafcad.com](https://graphql-docs.leafcad.com). La référence d'API montre tous les « types », « mutations », « queries » et « subscriptions » possibles qui sont disponibles.

## Principes de conception

Sous le capot, notre API GraphQL utilise un ORM appelé Prisma, qui gère les tâches communes et crée les relations souhaitées entre tous les différents types. Nous vous suggérons de vous familiariser avec la syntaxe de Prisma afin de comprendre comment lire la documentation et construire vos queries et mutations.

La plupart des types ont deux (2) queries, trois (3) mutations et parfois une subscription pour recevoir les mises à jour en temps réel. Certaines exceptions peuvent sont présente.

### Syntaxe des queries

Les queries peuvent récupérer un (1) nœud spécifique ou plusieurs nœuds selon la clause where. Par exemple, vous pouvez récuperer un civil spécifique de cette manière :

```graphql
query civilian(where: { id: "<ID>" }) {
  id
  firstName
  lastName
}
```

ou récuperer tous les civils d'un utilisateur donné avec son identifiant Steam :

```graphql
query civilians(where: { creatorSteamId: "<steamID64>" }) {
  id
  firstName
  lastName
}
```

Remarquez que la query qui permet d'obtenir un nœud spécifique est singulière, alors que celle qui permet d'obtenir plusieurs nœuds est plurielle. La clause where doit pouvoir sélectionner un nœud unique lors d'une requête à nœud singulier et la requête qui peut rechercher des nœuds multiples peut le faire avec une clause where plus large qui n'est pas nécessairement unique.

### Syntaxe des mutations

Les mutations sont très simples. Elles sont composées d'un mot-clé d'action comme create, update et delete, suivi du nom du type. Par exemple, voici les mutations pour le type civil.

```graphql
type Mutation {
  createCivilian(data: CivilianCreateInput!): Civilian!

  updateCivilian(
    data: CivilianUpdateInput!
    where: CivilianWhereUniqueInput!
  ): Civilian!

  deleteCivilian(where: CivilianWhereUniqueInput!): Civilian!
}
```

Nous utilisons des « inputs » pour les types des paramètres. Nous vous suggérons d'utiliser la [Référence d'API](https://graphql-docs.leafcad.com) pour connaître les spécificités de toutes les entrées que vous pourriez rencontrer puisqu'elles sont basées sur les types des champs mutables.

:::warning Mutations non autorisées
Certaines mutations entraînent une erreur lorsque vous essayez de les utiliser, suggérant que vous n'êtes pas autorisé. C'est tout à fait normal, certaines mutations sont connectées à notre système de facturation par exemple, et nous n'autorisons pas les communautés à accéder directement à ces mutations.
:::

#### Codes d'erreur

Lors d'une requête, il se peut que vous vous retrouviez avec une erreur. Nous utilisons toujours la même syntaxe lorsque nous renvoyons une erreur.

L'objet d'erreur ressemblera à ceci :

```json
{
  "data": null,
  "errors": [
    {
      "message": "You are not authenticated!",
      "locations": [
        {
          "line": 2,
          "column": 3
        }
      ],
      "path": ["updateCivilian"],
      "extensions": {
        "code": "UNAUTHENTICATED"
      }
    }
  ]
}
```

| Variable   | Description                                                                                                                                                              |
| ---------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| data       | Toujours `nul` car aucun nœud ou donnée ne peut être renvoyé                                                                                                             |
| errors     | Principal objet d'erreur, s'il est présent, il y a eu une erreur dans la demande                                                                                         |
| message    | Un message convivial, en anglais uniquement, expliquant l'erreur                                                                                                         |
| locations  | Où l'erreur s'est produite dans l'API, cela n'est pas important pour vous                                                                                                |
| path       | Le nom de la mutation qui à lancé l'erreur                                                                                                                               |
| extensions | Extensions de l'objet d'erreur                                                                                                                                           |
| code       | Le [code d'erreur](/en/api/error-codes) interne de LeafCAD. Ce code d'erreur est normalisé et documenté. Utilisez ceci pour savoir ce qui s'est passé avec votre requête |

[List of all the error codes](/en/api/error-codes)

### Syntaxe des subscriptions

Tous les types n'ont pas de subscriptions, mais certains en ont une. Nous vous suggérons de jeter un coup d'œil à la référence d'API si vous voulez voir quels types en ont. La subscription est assez simple aussi, une subscription vous permettra seulement de recevoir une mise à jour sur un noeud spécifique, donc le nom de la subscription est toujours singulier et similaire sinon identique au type. Par exemple, voici la subscription civil :

```graphql
type Subscription {
  civilian(where: CivilianSubscriptionWhereInput): CivilianSubscriptionPayload
}
```

Les inputs `*SubscriptionWhereInput` et `*SubscriptionPayload` sont toujours présents lors de l'utilisation d'une subscription mais ils sont adaptés au type (c'est-à-dire *Civilian*SubscriptionPayload).
Nous vous suggérons d'utiliser la référence d'API pour comprendre les `*SubscriptionWhereInput` et `*SubscriptionPayload`, mais ils suivent toujours la même structure :

```graphql
type CivilianSubscriptionPayload {
  mutation: MutationType!
  node: Civilian
  updatedFields: [String!]
  previousValues: CivilianPreviousValues
}
```

| Variable       | Description                                                                                               |
| -------------- | --------------------------------------------------------------------------------------------------------- |
| mutation       | Renvoie le type de mutation (`CREATED`, `UPDATED`, `DELETED`) qui a déclenché la souscription             |
| node           | Le nœud modifié est retourné par la mutation. Il sera toujours `null` si le nœud a été supprimé.          |
| updatedFields  | Liste des champs qui ont été mis à jour lors de la mutation                                               |
| previousValues | Les valeurs du nœud avant qu'il ne subisse une mutation. Elle sera toujours `null` si le nœud a été créé. |

## Authentification

Pour utiliser notre API GraphQL, vous aurez besoin d'une clé d'API. Cette clé est disponible dans la section générale du panel administratif de la communauté. Chaque requête faite à notre API doit inclure cette clé comme un `bearer token` dans l'en-tête `Authorization` de la requête.

Exemple:

```json
{
  "Authorization": "Bearer <votre clé d'API>"
}
```

## Lire la suite / Référence API

La référence d'API est disponible à l'adresse suivante : [https://graphql-docs.leafcad.com](https://graphql-docs.leafcad.com)
