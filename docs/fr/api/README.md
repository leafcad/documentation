---
title: Introduction à l'API
tags: [présentation, début, commencement, départ, intro, api]
lang: fr
---

# Introduction à l'API

LeafCAD est à la fine pointe de la technologie et utilise des languages de programmations relativement récents. À l'heure actuelle, nous avons publié une version publique de notre API interne en [GraphQL](https://graphql.org/). Nous avons prévu avoir un API parallèle REST afin de faciliter l'utilisation à tous, toutefois, sachez que l'API en GraphQL est celui qui sera le plus à jour et tester. Si vous souhaitez utiliser notre API pour créer des plugins ou autres, nous vous suggérons fortement de développer des connaisances avec cette nouvelle technologie.

## Ressources d'apprentissage pour GraphQL

Dans le cas où vous ne soyez pas familié avec GraphQL, nous vous suggérons les ressources éducatives suivantes:

- [https://graphql.org/learn/](https://graphql.org/learn/)
- [https://www.howtographql.com/](https://www.howtographql.com/)
- [https://v1.prisma.io/docs/1.34/](https://v1.prisma.io/docs/1.34/)
- Youtube
- Google
