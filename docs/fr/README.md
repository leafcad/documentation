---
home: true
heroImage: //cdn.leafcad.com/images/logos/leafcad-logos/Logo%20LeafCAD.png
heroText: LeafCAD Docs
tagline: Le meilleur CAD/MDT pour votre communauté
actionText: Documentation de l'API →
actionLink: /api/
features:
  - title: Personnalisable
    details: Vous pouvez personnaliser vos codes radio, civils, véhicules et bien plus encore.
  - title: Pour les communautés
    details: Équipez votre communauté de la meilleure solution CAD/MDT au monde!
  - title: Multilingue
    details: Plusieurs langues pour répondre aux besoins de tous vos membres.
footer: Documentation LeafCAD | Droit d'auteurs © 2020-présent LeafCAD
---
