---
title: GraphQL API
tags: [learn, understand, graphql, endpoint, endpoints]
lang: en
---

# GraphQL API

The GraphQL API is open to the public at [https://api.leafcad.com](https://api.leafcad.com). Any queries on this main endpoint must be formatted to follow the GraphQL standard. Most of the queries must be made using an API Key to authenticate your community.

This documentation serves as a support to the API reference which you can access at [https://graphql-docs.leafcad.com](https://graphql-docs.leafcad.com). The API reference shows all the possible types, mutations, queries and subscriptions that are available.

## Design Principles

Under the hood, our GraphQL API uses an ORM called Prisma, which handles the common tasks and creates the desired relationships between all the different types. We suggest you get familiar with Prisma's syntax to understand how to read the documentation and build your queries and mutations.

Most types have two (2) queries, three (3) mutations and sometimes a subscription to receive real-time updates. Some exceptions may occur.

### Queries Syntax

The queries can retrieve one (1) specific node or many nodes according to the where clause. For example, you can query a specific civilian like so:

```graphql
query civilian(where: { id: "<ID>" }) {
  id
  firstName
  lastName
}
```

or query all the civilians of a given user with its Steam ID:

```graphql
query civilians(where: { creatorSteamId: "<steamID64>" }) {
  id
  firstName
  lastName
}
```

Notice that the query which lets you obtain a specific node is singular, while the one that can retrieve multiple nodes is pluralized. The where clause must be able to select a unique node when making a singular node query and the multiple nodes query can search nodes can do it with a broader where clause which is not necessarily unique.

### Mutation Syntax

Mutations are very straight forward. They are composed of an action keyword like create, update and delete followed by the types name. For example here are the mutations for the civilian type.

```graphql
type Mutation {
  createCivilian(data: CivilianCreateInput!): Civilian!

  updateCivilian(
    data: CivilianUpdateInput!
    where: CivilianWhereUniqueInput!
  ): Civilian!

  deleteCivilian(where: CivilianWhereUniqueInput!): Civilian!
}
```

We are using inputs for the types of the parameters. We suggest that you use the [API Reference](https://graphql-docs.leafcad.com) to learn the specifics of any of the inputs you may encounter since they are based on the types of mutable fields.

:::warning Unauthorized Mutations
Some mutations will throw an error when you try to use them suggesting that you are not allowed. This is perfectly normal, some mutations are hooked into our billing system for example, and we do not allow communities to access those mutations directly.
:::

#### Error Codes

When making a request, you may end up with an error. We are always using the same syntax when returning an error.

The error object will look something like this:

```json
{
  "data": null,
  "errors": [
    {
      "message": "You are not authenticated!",
      "locations": [
        {
          "line": 2,
          "column": 3
        }
      ],
      "path": ["updateCivilian"],
      "extensions": {
        "code": "UNAUTHENTICATED"
      }
    }
  ]
}
```

| Variable   | Description                                                                                                                                                |
| ---------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------- |
| data       | Always `null` since no node or data can be returned                                                                                                        |
| errors     | Main error object, if it's present, there was an error with the request                                                                                    |
| message    | A user-friendly, English only, message explaining the error                                                                                                |
| locations  | Where the error occurred in the API, it's probably not important to you                                                                                    |
| path       | The name of the mutation that errored                                                                                                                      |
| extensions | Extensions to the error object                                                                                                                             |
| code       | The internal LeafCAD's [error code](/en/api/error-codes). This error code is standardized and documented. Use this to know what happened with your request |

[List of all the error codes](/en/api/error-codes)

### Subscription Syntax

Not all types have a subscription, but some of them have, we suggest you take a look at the API Reference if you want to see which type have one. The subscription is fairly simple too, a subscription will only allow you to receive an update on a specific node, therefore the subscription's name is always singular and similar if not the same as the type. For example, here is the civilian subscription:

```graphql
type Subscription {
  civilian(where: CivilianSubscriptionWhereInput): CivilianSubscriptionPayload
}
```

The `*SubscriptionWhereInput` and `*SubscriptionPayload` are always present when using a subscription but it is tailored to the type (i.e *Civilian*SubscriptionPayload).
We suggest you use the API Reference to understand the SubscriptionWhereInput, SubscriptionPayload, but it always follows the same structure:

```graphql
type CivilianSubscriptionPayload {
  mutation: MutationType!
  node: Civilian
  updatedFields: [String!]
  previousValues: CivilianPreviousValues
}
```

| Variable       | Description                                                                                        |
| -------------- | -------------------------------------------------------------------------------------------------- |
| mutation       | Returns the type of the mutation (`CREATED`, `UPDATED`, `DELETED`) that triggered the subscription |
| node           | The modified node returned by the mutation. It will always be `null` if the node was deleted.      |
| updatedFields  | List of the fields that were updated in the mutation                                               |
| previousValues | The values of the node before it was mutated. It will always be `null` if the node was created.    |

## Authentication

To use our GraphQL API, you will need an API key. This key is available under the general section in the administrative panel of the community. Each request made to our API needs to include this key as a `bearer token` in the `Authorization` header of the request.

Example:

```json
{
  "Authorization": "Bearer <Your API Key>"
}
```

## Read More / API Reference

The API Reference is available at: [https://graphql-docs.leafcad.com](https://graphql-docs.leafcad.com)
